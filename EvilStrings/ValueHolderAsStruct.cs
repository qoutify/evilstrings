﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvilStrings
{
    public struct ValueHolderAsStruct
    {
        public int ElementId { get; }
        public int VehicleId { get; }
        public int Term { get; }
        public int Mileage { get; }
        public decimal Value { get; }

        public ValueHolderAsStruct(int elementId, int vehicleId, int term, int mileage, decimal value)
        {
            ElementId = elementId;
            VehicleId = vehicleId;
            Term = term;
            Mileage = mileage;
            Value = value;
        }

        public override string ToString()
        {
            return ElementId + "," + VehicleId + "," + Term + "," + Mileage + "," + Value;
        }
    }
}
