﻿using System.IO;
using System.Runtime.CompilerServices;

namespace EvilStrings
{
    public class DataProvider
    {
        private readonly int _bufferSize;
        private readonly FileStream _file;
        private byte[] _currentBuffer;
        private readonly byte[] _buffer1;
        private readonly byte[] _buffer2;
        private int _pos;
        private int _noBytesInCurrentBuffer;
        private int _noBytesInNextBuffer;

        private bool _isFetcingFromLastBuffer;

        public DataProvider(string fileName, int bufferSize)
        {
            _bufferSize = bufferSize;
            _file = File.OpenRead(fileName);

            _buffer1 = new byte[bufferSize];
            _buffer2 = new byte[bufferSize];

            _noBytesInCurrentBuffer = _file.Read(_buffer1, 0, _bufferSize);
            _noBytesInNextBuffer = _file.Read(_buffer2, 0, _bufferSize);

            _currentBuffer = _buffer1;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public byte GetNext()
        {
            if (_pos < _bufferSize)
            {
                return _currentBuffer[_pos++];
            }

            this.ReadFromFile();
            _pos = 0;

            return _currentBuffer[_pos++];
        }

        public void SkipNext()
        {
            if (_pos < _bufferSize)
                _pos++;
            else
            {
                ReadFromFile();
                _pos = 0;
            }
        }

        private void ReadFromFile()
        {
            _noBytesInCurrentBuffer = _noBytesInNextBuffer;
            _noBytesInNextBuffer = _file.Read(_currentBuffer, 0, _bufferSize);
            if (_noBytesInNextBuffer < 1)
            {
                _isFetcingFromLastBuffer = true;
            }

            if (_currentBuffer == _buffer1)
            {
                _currentBuffer = _buffer2;
            }
            else
            {
                _currentBuffer = _buffer1;
            }
        }

        public bool StillDataToRead()
        {
            return !_isFetcingFromLastBuffer || _pos < _noBytesInCurrentBuffer - 2;
        }

        ~DataProvider()
        {
            if (_file != null)
                _file.Close();
        }
    }
}