﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;

namespace EvilStrings
{
    class Program
    {
        private const int BufferSize = 1024 * 4;

        static void Main(string[] args)
        {
            AppDomain.MonitoringIsEnabled = true;

            var dataProvider = new DataProvider(@"..\..\example-input.csv", BufferSize);
            var lineParser = new LineParser(dataProvider);

            while (dataProvider.StillDataToRead())
            {
                lineParser.ParseLine();
            }

            Console.WriteLine(
                $"Took: {AppDomain.CurrentDomain.MonitoringTotalProcessorTime.TotalMilliseconds:#,###} ms");
            Console.WriteLine($"Allocated: {AppDomain.CurrentDomain.MonitoringTotalAllocatedMemorySize / 1024:#,#} kb");
            Console.WriteLine($"Peak Working Set: {Process.GetCurrentProcess().PeakWorkingSet64 / 1024:#,#} kb");

            Console.ReadKey();
        }
    }
}
