﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace EvilStrings
{
    public class LineParser : ILineParser
    {
        private byte[] _buffer = new byte[10];
        private int _length;

        private DataProvider _dp;

        private int currentIntResult;
        private decimal currentDecResult;
        private int currentIndex;
        private byte tmp;

        public LineParser(DataProvider dp)
        {
            this._dp = dp;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void ParseLine()
        {
            if (_dp.GetNext() != 'M' || _dp.GetNext() != 'N' || _dp.GetNext() != 'O')
            {
                SkipRestOfLine();
            }

            this._dp.SkipNext();

            var t = new ValueHolderAsStruct(this.ParseInt(), this.ParseInt(), this.ParseInt(), this.ParseInt(), this.ParseDecimal());

            SkipRestOfLine();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void SkipRestOfLine()
        {
            do
            {
                tmp = this._dp.GetNext();
            } while (tmp != 10 && tmp != 13);
            this._dp.SkipNext();
        }

        private static int[] exps = { 0, 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000 };
        private static decimal[] exps2 = { 0.1M, 0.01M, 0.001M, 0.001M, 0.001M, 0.001M, 0.001M, 0.0001M, 0.00001M, 0.000001M, 0.0000001M };

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int ParseInt()
        {
            this._length = 0;
            while (true)
            {
                _buffer[this._length] = _dp.GetNext();
                if (_buffer[this._length] == ',')
                    break;
                this._length++;
            }

            this.currentIntResult = 0;
            for (this.currentIndex = 0; this.currentIndex < this._length - 1; this.currentIndex++)
            {
                this.currentIntResult += (_buffer[this.currentIndex] - 0x30) * exps[this._length - this.currentIndex];
            }
            return this.currentIntResult + (_buffer[this.currentIndex] - 0x30);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private decimal ParseDecimal()
        {
            this._length = 0;

            while (true)
            {
                var c = _dp.GetNext();
                if (c == '.' || c == ',')
                    break;
                this._buffer[this._length++] = c;
            }

            this.currentDecResult = 0;
            for (this.currentIndex = 0; this.currentIndex < this._length - 1; this.currentIndex++)
            {
                this.currentDecResult += (_buffer[this.currentIndex] - 0x30) * exps[this._length - this.currentIndex];
            }

            this.currentDecResult += (_buffer[this.currentIndex] - 0x30);

            this._length = 0;
            if (this._buffer[this._length] != ',')
            {
                while (true)
                {
                    _buffer[this._length] = _dp.GetNext();
                    if (_buffer[this._length] == ',')
                        break;
                    this._length++;
                }

                for (this.currentIndex = 0; this.currentIndex < this._length; this.currentIndex++)
                {
                    this.currentDecResult += (_buffer[this.currentIndex] - 0x30) * exps2[this.currentIndex];
                }
            }

            return this.currentDecResult;
        }
    }
}
